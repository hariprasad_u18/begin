provider "aws" {
  access_key = "AKIAUYNQKXCSWPDVD4N7"
  secret_key = "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"

  region     = "ap-south-1"
  
}



resource "aws_vpc" "project-vpc" {
  cidr_block           = "10.0.0.0/16"
  instance_tenancy     = "default"
  enable_dns_support   = "true"
  enable_dns_hostnames = "true"
  enable_classiclink   = "false"
  tags = {
    Name = "projectvpc"
  }
}

resource "aws_security_group" "project-nsg" {
name = "project-nsg"
vpc_id = "${aws_vpc.project-vpc.id}"
    ingress {
        cidr_blocks = ["0.0.0.0/0"]
      from_port = 22
        to_port = 22
        protocol = "tcp"

      }

  egress {
   from_port = 0
   to_port = 0
   protocol = "-1"
   cidr_blocks = ["0.0.0.0/0"]
 }
}


resource "aws_subnet" "public-subnet-1" {
  vpc_id                  = aws_vpc.project-vpc.id
  cidr_block              = "10.0.1.0/24"
  map_public_ip_on_launch = "true"
  availability_zone       = "ap-south-1a"

  tags = {
    Name = "public-subnet-1"
  }
}

resource "aws_subnet" "private-subnet-2" {
  vpc_id                  = aws_vpc.project-vpc.id
  cidr_block              = "10.0.2.0/24"
  map_public_ip_on_launch = "false"
  availability_zone       = "ap-south-1a"

  tags = {
    Name = "private-subnet-2"
  }
}




resource "aws_internet_gateway" "project-gw" {
  vpc_id = aws_vpc.project-vpc.id

  tags = {
    Name = "project-gw"
  }
}


resource "aws_route_table" "project-public" {
  vpc_id = aws_vpc.project-vpc.id
  route {
    cidr_block = "0.0.0.0/0"
    gateway_id = aws_internet_gateway.project-gw.id
  }

  tags = {
    Name = "project-public-1"
  }
}


resource "aws_route_table_association" "public-1-a" {
  subnet_id      = aws_subnet.public-subnet-1.id
  route_table_id = aws_route_table.project-public.id
}

resource "aws_eip" "elastic-ip"{
  vpc = "true"
  depends_on = [aws_internet_gateway.project-gw]
  tags = {
    Name = "NAT-eip"
  }
}

resource "aws_nat_gateway" "nat"{
  allocation_id = aws_eip.elastic-ip.id
  subnet_id = aws_subnet.public-subnet-1.id

  tags = {

    Name = "Nat gateway"
  }
  depends_on = [aws_internet_gateway.project-gw]
}

resource "aws_route_table" "private_rt"{
  vpc_id = aws_vpc.project-vpc.id
  route {
    cidr_block = "0.0.0.0/0"
    gateway_id = aws_nat_gateway.nat.id
  }
  tags={
    Name = "Private route table"
  }
}

resource "aws_route_table_association" "private"{
  depends_on = [aws_route_table.private_rt]
  subnet_id = "aws_subnet.private-subnet-2.id"
  route_table_id = "aws_route_table.private_rt.id"
}

resource "aws_instance" "public-instance" {
  ami           = "ami-08ee6644906ff4d6c"
  instance_type = "t2.micro"
  subnet_id = "${aws_subnet.public-subnet-1.id}"
  key_name = "new"
  security_groups = ["${aws_security_group.project-nsg.id}"]
  associate_public_ip_address = "true"
  
  tags = {
    Name = "public"
  }
}

resource "aws_instance" "private-instance" {
  ami           = "ami-08ee6644906ff4d6c"
  instance_type = "t2.micro"
  subnet_id = "${aws_subnet.private-subnet-2.id}"
  key_name = "new"
  security_groups = ["${aws_security_group.project-nsg.id}"]
  associate_public_ip_address = "false"
  
  tags = {
    Name = "private"
  }
}

